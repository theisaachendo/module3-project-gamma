from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
import os
import json
import requests
import logging
import inspect


app = FastAPI()

#Logger settings
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST", "http://localhost:3000")
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def api_headers():
    headers = {
        'X-Api-Key': '5b58bf75c21d4d22b817d819986bd7ef',
    }

    return headers


def execute_request(url, method, headers=None, params=None, body=None):
    if method == "GET":
        response = requests.get(url, headers=headers, params=params)
    elif method == "POST":
        response = requests.post(url, headers=headers, body=body)

    if response.status_code not in [200, 201]:
        error_info = response.json()
        logger.error(f'Error response from server: {error_info}')
        raise HTTPException(status_code=400, detail=str(error_info))
    
    return json.loads(response.content)


def top_headlines_business():
    try:
        headers = api_headers()
        body = {
            'country': 'us',
            'category': 'business',
            'q': 'bitcoin',
        }
        data = execute_request(
            'https://newsapi.org/v2/top-headlines',
            'GET',
            headers=headers,
            params=body
        )
        logger.info("Successfully obtained trending business news")
        return data
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))


@app.get("/news/guest")
def get_news_trends():
    try:
        trending_guest_news = top_headlines_business()
        return {'trending': trending_guest_news}
    except Exception as e:
        logger.error(e)
        raise HTTPException(status_code=400, detail=str(e))


@app.get("/news")
def get_news_logos_list():
    return {"message": "Hello World"}


@app.get("/news/topics")
def get_topical_news_list():
    return {"message": "Hello World"}


@app.get("/news/topicdetails")
def get_topical_news_details():
    return {"message": "Hello World"}


@app.post("/user/create")
def post_user():
    return {"message": "Hello World"}


@app.get("/user/details")
def get_user_details():
    return {"message": "Hello World"}


@app.get("/news/topics/user/filteredtopics")
def get_user_filtered_news_list():
    return {"message": "Hello World"}


@app.post("/user/signin")
def post_user_signin():
    return {"message": "Hello World"}


@app.post("/user/signout")
def post_user_signout():
    return {"message": "Hello World"}
